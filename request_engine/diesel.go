package request_engine

import . "github.com/gin-gonic/gin"

// Diesel is a custom engine format that conforms to golang standards.
type Diesel struct {
	*Engine
}

// IDieselMethods identify the API requests.
type IDieselMethods interface {
	Get(string, ...HandlerFunc) IRoutes
	Post(string, ...HandlerFunc) IRoutes
	Put(string, ...HandlerFunc) IRoutes
	Delete(string, ...HandlerFunc) IRoutes
	Options(string, ...HandlerFunc) IRoutes
	Patch(string, ...HandlerFunc) IRoutes
	Head(string, ...HandlerFunc) IRoutes
}

// GET.
func (group *Diesel) Get(path string, h ...HandlerFunc) IRoutes {
	return group.GET(path, h...)
}

// POST
func (group *Diesel) Post(path string, h ...HandlerFunc) IRoutes {
	return group.POST(path, h...)
}

// PUT
func (group *Diesel) Put(path string, h ...HandlerFunc) IRoutes {
	return group.PUT(path, h...)
}

// DELETE
func (group *Diesel) Delete(path string, h ...HandlerFunc) IRoutes {
	return group.DELETE(path, h...)
}

// OPTIONS
func (group *Diesel) Options(path string, h ...HandlerFunc) IRoutes {
	return group.OPTIONS(path, h...)
}

// PATCH
func (group *Diesel) Patch(path string, h ...HandlerFunc) IRoutes {
	return group.PATCH(path, h...)
}

// HEAD
func (group *Diesel) Head(path string, h ...HandlerFunc) IRoutes {
	return group.HEAD(path, h...)
}
