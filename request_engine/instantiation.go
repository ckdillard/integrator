package request_engine

import "github.com/gin-gonic/gin"

func Init() *Diesel {
	engine := gin.Default()
	diesel := &Diesel{
		Engine: engine,
	}

	return diesel
}
