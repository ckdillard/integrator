package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	engine "gitlab.com/ckdillard/integrator/request_engine"
)

func main() {
	fmt.Println("Hello, World!")

	r := engine.Init()
	r.Get("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "pong"})
	})

	_ = r.Run()
}
